//
//  ViewController.swift
//  SmallVille
//
//  Created by Anuj Patel on 11/11/18.
//  Copyright © 2018 Anuj Patel. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

class ViewController: UIViewController, GIDSignInUIDelegate{
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var customFBButton: UIButton!
    @IBOutlet weak var customGoogleButton: UIButton!
    
    let customToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2lkZW50aXR5dG9vbGtpdC5nb29nbGVhcGlzLmNvbS9nb29nbGUuaWRlbnRpdHkuaWRlbnRpdHl0b29sa2l0LnYxLklkZW50aXR5VG9vbGtpdCIsImlhdCI6MTU0MzU2NDc5NSwiZXhwIjoxNTQzNTY4Mzk1LCJpc3MiOiJmaXJlYmFzZS1hZG1pbnNkay1yczczZUBwcm9qZWN0c21hbGx2aWxsZS5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsInN1YiI6ImZpcmViYXNlLWFkbWluc2RrLXJzNzNlQHByb2plY3RzbWFsbHZpbGxlLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwidWlkIjoiaVBJYUhCNGhSVWducFVlazRUTHNhYXd3V1F2MSJ9.d68GYNxaWbT9GGWe9R0RF-uBfIFPl0CjCwW6huvLztQVaAv1-0VnNeOnaHeyKLAzGkbl5EfqfbD3CKy-U7E_-nHiWvlgABjjmLJm-fHgTGwZmzRYNRVgbs96rcBRqwA5EZScJ7lKukf_878nSVwv3mAfuIVgBr4mzueBm84YVtsGMA1wALTIMfuCezmItdLZ7ddWD2zf-WvSUSALYYUBarAMKrrDEBo69MSruT-z0uuY5IlihfrOn3UPuNPHjobAC6axNAznwbnoexCGUsLwr58ivtg94MW2V4o3zwwbbw3adkg7-GHj3K93ZQ9WsiSMEQZOwRBJq8kVZWp6TeyQNA"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        GIDSignIn.sharedInstance()?.uiDelegate = self
        
        
        // Adding custom Google Login Button
        customGoogleButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        customGoogleButton.addTarget(self, action:  #selector(handleCustomGoogleLogin), for: .touchUpInside)
        
        
        // Adding custom FB Login Button
        customFBButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        customFBButton.addTarget(self, action: #selector(handleCustomFBLogin), for: .touchUpInside)
    }
    
    // Call the Google SDK using the custom button
    @objc func handleCustomGoogleLogin(){
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    // Call the FB SDK using the custom button
    @objc func handleCustomFBLogin() {
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, err) in
            if err != nil {
                print("Custom FB login failed :" , err ?? "")
                return
            }
                print("Custom FB login successful")
                self.showEmailAddress()
        }
    }
 

    // Query FB for email as well as generate a Firebase User
    func showEmailAddress(){
        
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                print("Error when creating Firebase Facebook user: \(error)")
                return
            }
            
            print("***** Logged in to Firebase with below Facebook credentials *****")
            print("Email: ", authResult?.user.email ?? "" )
            print("Display Name: ", authResult?.user.displayName ?? "" )
            print("UID: ", authResult?.user.uid ?? "")
            
            // User is signed in
                // ...
        }
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"])?.start(completionHandler: { (connection, result, err) in
            if err != nil{
                print("Failed to complete graph request: ", err ?? "")
                return
            }
            
            print("Printing Facebook Graph result: ", result ?? "")
            
        })
        
        getFirebaseIDToken()
        
    }

    // Sign in with Email-Password
    @IBAction func customSignIn(_ sender: Any) {

        Auth.auth().signIn(withCustomToken: customToken ?? "") { (currentUser, error) in
            // ...
            if let error = error{
                print("Failed to generate Firebase ID Token on the client with error: ", error ?? "")
                return
            }
            self.getFirebaseIDToken()
        }
        /*
        
        if (((email.text?.isEmpty)!) || ((password.text?.isEmpty)!) ){
               print("Email or password is empty")
            } else if ( (email.text == "test@email.com") && (password.text == "password") ) {
                print("Sign in successful!")
                
           
            
            
            } else {
                print("Sign in failed!")
            print("Email : \(email.text ?? "")")
            print("Password : \(password.text ?? "")")
        }
         */
    }

    
    func getFirebaseIDToken(){
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                print("Error while generating Firebase ID Token after sucessfully signing in: ", error ?? "")
                return
            }
            print("The ID Token to be sent to the beckend for validation is: ", idToken ?? "")
            print("***** Printing Firebase current user details ****")
            print(currentUser?.email ?? "")
            // Send token to your backend via HTTPS
            // ...
        }
        
    }
    
    

}

